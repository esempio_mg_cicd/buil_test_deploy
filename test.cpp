#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "fattoriale.h"


TEST_CASE ("Fattoriale ") {
    REQUIRE( fattoriale(1) == 1 );
    REQUIRE( fattoriale(2) == 2 );
    REQUIRE (fattoriale (5) == 120);
    REQUIRE (fattoriale(10) == 3628800);
}
