all: eseguibile_test app

eseguibile_test: test.o fattoriale.o
	g++ -o eseguibile_test test.o fattoriale.o

app: main.o fattoriale.o
	g++ -o app main.o fattoriale.o

main.o: main.cpp fattoriale.h
	g++ -c main.cpp

test.o: test.cpp fattoriale.h
	g++ -c test.cpp

fattoriale.o: fattoriale.cpp fattoriale.h
	g++ -c fattoriale.cpp